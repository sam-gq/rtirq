Source: rtirq
Section: misc
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper-compat (= 10)
Standards-Version: 3.9.8
Vcs-Git: https://salsa.debian.org/multimedia-team/rtirq.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/rtirq
Homepage: https://github.com/rncbc/rtirq/

Package: rtirq-init
Architecture: all
Depends:
 ${misc:Depends}
Description: startup script for realtime-preempt enabled kernels
 This package contains a script to prioritize IRQ thread handlers
 on a kernel patched with realtime-preempt patch from Ingo Molnar.
 The script takes advantage of the fact that realtime kernels use
 threads for IRQs management, and as such these threads (like any
 other thread running on your system) can be given maximum
 priority in an effort to minimize the latency of audio
 peripherals.
 .
 The script identifies the audio devices present on the machine
 and raises the priority of the threads that handle the IRQs of
 such devices.